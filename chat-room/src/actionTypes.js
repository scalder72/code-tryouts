export const USER_LOGGED_IN = "@@auth/USER_LOGGED_IN";
export const USER_LOGGED_OUT = "@@auth/USER_LOGGED_OUT";
export const USERS_CHANGED = "@@users/USERS_CHANGED";
export const ADD_MESSAGES = "@@messages/ADD_MESSAGES";
export const CREATE_MESSAGE = "@@messages/CREATE_MESSAGE";
