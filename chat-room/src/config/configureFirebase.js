import * as firebase from "firebase";

import compose from "lodash/fp/compose";
import isFunction from "lodash/fp/isFunction";
import filter from "lodash/fp/filter";
import map from "lodash/fp/map";

import * as firebaseListeners from "./../firebase";

var config = {
    apiKey: "AIzaSyD497PI8nYYhOpWG0iEwmT8NBU0I2iQ5W0",
    authDomain: "chatproject-f7935.firebaseapp.com",
    databaseURL: "https://chatproject-f7935.firebaseio.com",
    storageBucket: "chatproject-f7935.appspot.com",
    messagingSenderId: "60471913516"
};

firebase.initializeApp(config);

export const connectFirebase = (store) => {
    const passStoreToListeners = compose(
        map((listener) => listener(store.dispatch, store.getState)),
        filter(isFunction)
    );

    passStoreToListeners(firebaseListeners);
};
