import { createStore, compose, applyMiddleware } from "redux";
import thunk from "redux-thunk";

import { reducer } from "./../reducer";

const includeDevTools = () => {
    if (process && process.env && process.env.NODE_ENV !== "production" && window && window.devToolsExtension) {
        return window.devToolsExtension();
    }

    return f => f;
};

export const configureStore = () => createStore(
    reducer,
    {},
    compose(
        applyMiddleware(thunk),
        includeDevTools()
    )
);
