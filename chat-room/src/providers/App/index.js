import { connect } from "react-redux";
import App from "./../../components/App";

import { signIn, signUp } from "./../../asyncActions";
import { getUiState } from "./../../reducer";


export default connect(getUiState, { signIn, signUp })(App);
