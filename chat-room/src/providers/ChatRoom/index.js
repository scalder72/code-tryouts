import { connect } from "react-redux";
import ChatRoom from "./../../components/ChatRoom";

import { createMessage, signOut } from "./../../asyncActions";
import { getChatRoomState } from "./../../reducer";


export default connect(getChatRoomState, { createMessage, signOut })(ChatRoom);
