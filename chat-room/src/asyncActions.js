import * as firebase from "firebase";
import moment from "moment";

export const createOrUpdateUser = (user) => () => {
    firebase.database()
        .ref("users/" + user.uid)
        .set({
            email: user.email,
            uid: user.uid
        });
};

export const createMessage = ({ message, user }) => () => {
    var ref = firebase.database().ref().child("messages");

    ref.push({
        text: message,
        author: user.uid,
        createDate: moment.utc().toISOString()
    });
};

export const signIn = ({ email, password }) => (dispatch) => {
    firebase.auth()
        .signInWithEmailAndPassword(email, password)
        .then(user => dispatch(createOrUpdateUser(user)));
};

export const signUp = ({ email, password }) => (dispatch) => {
    firebase.auth()
        .createUserWithEmailAndPassword(email, password)
        .then(user => dispatch(createOrUpdateUser(user)));
};

export const signOut = () => () => {
    firebase.auth().signOut();
};
