import { combineReducers } from "redux";
import { handleActions } from "redux-actions";
import { createSelector } from "reselect";

import * as actionTypes from "./actionTypes";

export const reducer = combineReducers({
    uiState: handleActions({
        [actionTypes.USER_LOGGED_IN]: (state) => ({
            ...state,
            isUserLoggedIn: true,
            isAppReady: true
        }),
        [actionTypes.USER_LOGGED_OUT]: (state) => ({
            ...state,
            isUserLoggedIn: false,
            isAppReady: true
        }),
        [actionTypes.USERS_CHANGED]: (state) => {
            if (state.isUsersLoaded) {
                return state;
            }

            return { ...state, isUsersLoaded: true };
        },
        [actionTypes.ADD_MESSAGES]: (state) => {
            if (state.isMessagesLoaded) {
                return state;
            }

            return { ...state, isMessagesLoaded: true };
        },
    }, {
        isMessagesLoaded: false,
        isUsersLoaded: false,
        isUserLoggedIn: false,
        isAppReady: false
    }),

    currentUser: handleActions({
        [actionTypes.USER_LOGGED_IN]: (state, { payload }) => payload.user,
        [actionTypes.USER_LOGGED_OUT]: (state) => null,
    }, null),

    users: handleActions({
        [actionTypes.USERS_CHANGED]: (state, { payload }) => {
            return { ...payload.users };
        }
    }, {}),

    messages: handleActions({
        [actionTypes.ADD_MESSAGES]: (state, { payload }) => {
            const messages = payload.messages;
            return Object.keys(messages).map(key => messages[key]);
        },
        [actionTypes.CREATE_MESSAGE]: (state, { payload }) => [...state, payload.message]
    }, [])
});

export const getUiState = (state) => state.uiState;
export const getUsers = (state) => state.users;
export const getCurrentUser = (state) => state.currentUser;
export const getMessages = (state) => state.messages;

export const getChatRoomState = createSelector(
    getUsers,
    getCurrentUser,
    getMessages,
    (users, currentUser, messages) => ({
        users,
        currentUser,
        messages
    })
);

export default reducer;
