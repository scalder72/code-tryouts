export const styles = {
    overflow: {
        position: "fixed",
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,

        background: "rgba(255, 255, 255, .7)"
    },

    loader: {
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)"
    }
};

export default styles;
