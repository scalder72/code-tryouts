import React, { Component } from "react";
import injectStyles from "react-jss";
import cx from "classnames";

import "./ripple.css"
import styles from "./styles";

class Loader extends Component {
    render() {
        const { sheet: { classes }} = this.props;
        return (
            <div className={classes.overflow}>
                <div className={cx("uil-ripple-css", classes.loader)}>
                    <div />
                    <div />
                </div>
            </div>
        );
    }
}

export default injectStyles(styles)(Loader);
