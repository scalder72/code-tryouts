import React, { Component, PropTypes } from "react";
import { findDOMNode } from "react-dom";
import { Form, FormGroup, FormControl, Col, Button } from "react-bootstrap";

import injectJss from "react-jss";
import { styles } from "./styles";

class AuthForm extends Component {
    static propTypes = {
        onSignIn: PropTypes.func.isRequired,
        onSignUp: PropTypes.func.isRequired
    };

    onSignIn = () => {
        const { onSignIn } = this.props;
        const formElements = this._formNode.elements;

        var email = formElements.email.value;
        var password = formElements.password.value;

        onSignIn({
            email,
            password
        });
    };

    onSignUp = () => {
        const { onSignUp } = this.props;
        const formElements = this._formNode.elements;

        var email = formElements.email.value;
        var password = formElements.password.value;

        onSignUp({
            email,
            password
        });
    };

    render() {
        const { sheet: { classes } } = this.props;

        return (
            <div className={classes.authWrapper}>
                <h2 className={classes.title}>Добро пожаловать в чат!</h2>

                <p className={classes.description}>
                    войдите под уже созданным аккаунтом или создайте новый
                </p>

                <Form onSubmit={evt => { evt.preventDefault(); }}
                      ref={component => { this._formNode = findDOMNode(component); }}
                >
                    <FormGroup bsClass={classes.formGroup}>
                        <Col sm={2}>
                            <div className={classes.labelGroup}>
                                Email
                            </div>
                        </Col>
                        <Col sm={10}>
                            <FormControl type="email" name="email" placeholder="Email" />
                        </Col>
                    </FormGroup>

                    <FormGroup bsClass={classes.formGroup}>
                        <Col sm={2}>
                            <div className={classes.labelGroup}>
                                Пароль
                            </div>
                        </Col>
                        <Col sm={10}>
                            <FormControl type="password" name="password" placeholder="Пароль" />
                        </Col>
                    </FormGroup>

                    <FormGroup bsClass={classes.formGroup}>
                        <Col smOffset={2} sm={10}>
                            <Button type="button" onClick={this.onSignIn} className={classes.button}>
                                Войти
                            </Button>

                            <Button type="button" onClick={this.onSignUp} className={classes.button}>
                                Создать
                            </Button>
                        </Col>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}

export default injectJss(styles)(AuthForm);
