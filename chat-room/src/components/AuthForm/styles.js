export const clearfix = {
    "&:before, &:after": {
        content: "' '",
        clear: "both",
        display: "table"
    }
};

export const styles = {
    authWrapper: {
        width: "40%",
        minWidth: 460,
        position: "absolute",
        top: "30%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        composes: ["well"],
        boxShadow: "0px 0px 10px rgba(0,0,0,.1)"
    },
    labelGroup: {
        paddingTop: 7,
        textAlign: "right"
    },
    formGroup: {
        marginTop: 10,
        extend: clearfix
    },
    button: {
        marginRight: 5
    },
    title: {
        textAlign: "center",
        margin: [[ 10, 0, 10 ]]
    },
    description: {
        composes: ["text-center", "text-muted"],
        marginBottom: 20
    }
};

export default styles;
