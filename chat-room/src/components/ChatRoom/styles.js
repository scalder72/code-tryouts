export const styles = {
    chatRoomWrapper: {
        width: "40%",
        minWidth: 460,
        margin: [[ 20, "auto" ]]
    },

    header: {
        textAlign: "right"
    },

    well: {
        composes: ["well"],
        boxShadow: "0px 0px 10px rgba(0,0,0,.1)"
    },

    message: {
        composes: ["well", "well-sm"],
        background: "#fff"
    },

    isCurrentUser: {
        backgroundColor: "#eff6ec",
    }
};

export default styles;
