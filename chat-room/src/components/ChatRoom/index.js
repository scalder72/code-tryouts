import React, { Component } from "react";
import injectStyles from "react-jss";
import cx from "classnames";
import debounce from "lodash/debounce";

import moment from "moment";
moment.locale("ru");

import { PageHeader, FormControl, Button } from "react-bootstrap";
import styles from "./styles";

class ChatRoom extends Component {
    componentDidMount() {
        this.scrollToPageBottom();
    }

    componentDidUpdate() {
        this.scrollToPageBottom();
    }

    scrollToPageBottom = debounce(() => {
        const bodyNode = document.querySelector("body");
        bodyNode.scrollTop = bodyNode.clientHeight;
    }, 50);

    onKeyUp = (evt) => {
        const { currentUser, createMessage } = this.props;

        const key = evt.keyCode || evt.wich;
        if (evt.ctrlKey && key === 13) {
            createMessage({ message: evt.target.value, user: currentUser });
        }
    };

    render() {
        const { users, currentUser, messages, sheet: { classes }, signOut } = this.props;

        return (
            <div className={classes.chatRoomWrapper}>
                <div className={classes.header}>
                    Добро пожаловать, { currentUser.email }
                    <Button bsStyle="link" onClick={signOut}>
                        Выйти
                    </Button>
                </div>
                
                <div className={classes.well}>
                    <PageHeader>Главный чат</PageHeader>

                    <p>
                        Участники чата: {Object.keys(users).map(key => users[key].email).join(", ")}
                    </p>

                    {messages.map(message => {
                        const isCurrentUser = message.author === currentUser.uid;
                        const messageClassName = cx(classes.message,  {[classes.isCurrentUser]: isCurrentUser });

                        return (
                            <div key={message.createDate} className={messageClassName}>
                                <div style={{fontSize: ".8em", marginBottom: 5 }}>
                                    <strong>
                                        {users[message.author].email}
                                    </strong>
                                    <span style={{ marginLeft: 5, color: "grey"}}>
                                    {moment(message.createDate).fromNow()}
                                </span>
                                </div>
                                {message.text}
                            </div>
                        );
                    })}

                    <FormControl componentClass="textarea"
                                 placeholder="Напишите сообщение. Для отправки нажмите `ctrl+enter`."
                                 onKeyUp={this.onKeyUp}
                    />
                </div>
            </div>
        );
    }
}

export default injectStyles(styles)(ChatRoom);
