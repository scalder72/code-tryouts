import React, { Component, PropTypes } from "react";

import Loader from "./Loader";
import AuthForm from "./AuthForm";
import ChatRoom from "./../providers/ChatRoom";

class App extends Component {
    static propTypes = {
        isUserLoggedIn: PropTypes.bool,
        isAppReady: PropTypes.bool,
        isMessagesLoaded: PropTypes.bool,
        isUsersLoaded: PropTypes.bool,

        signIn: PropTypes.func,
        signUp: PropTypes.func,
        signOut: PropTypes.func
    };

    render() {
        const { isUserLoggedIn, isAppReady, isMessagesLoaded, isUsersLoaded, signIn, signUp } = this.props;
        const showLoader = !isAppReady || (isUserLoggedIn && (!isMessagesLoaded || !isUsersLoaded));

        if (showLoader) {
            return <Loader />;
        }

        if (!isUserLoggedIn) {
            return (
                <AuthForm onSignIn={signIn} onSignUp={signUp} />
            );
        }

        return (
            <div>
                <ChatRoom />
            </div>

        );
    }
}

export default App;
