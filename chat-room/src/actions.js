import { createAction } from "redux-actions";
import * as actionTypes from "./actionTypes";

export const loggedIn = createAction(actionTypes.USER_LOGGED_IN);
export const loggedOut = createAction(actionTypes.USER_LOGGED_OUT);

export const usersChanged = createAction(actionTypes.USERS_CHANGED);

export const addMessages = createAction(actionTypes.ADD_MESSAGES);
export const createMessage = createAction(actionTypes.CREATE_MESSAGE);
