import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";

import { configureStore } from "./config/configureStore";
import { connectFirebase } from "./config/configureFirebase";

import App from "./providers/App";

import "bootstrap/dist/css/bootstrap.css";
import "./index.css";

const store = configureStore();
connectFirebase(store);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>
  ,
  document.getElementById("root")
);
