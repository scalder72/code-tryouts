import * as actions from "./../actions";
import * as firebase from "firebase";

export const onAuthStateChanged = (dispatch) => {
    firebase.auth().onAuthStateChanged(user => {
        if (user !== null) {
            dispatch(actions.loggedIn({ user }));
        } else {
            dispatch(actions.loggedOut());
        }
    });
};
