import * as firebase from "firebase";
import * as actions from "./../actions";

export const onUsersChanged = (dispatch) => {
    const refUsers = firebase.database().ref().child("users");
    refUsers.once("value", snap => {
        dispatch(actions.usersChanged({ users: snap.val() }));
    });
};
