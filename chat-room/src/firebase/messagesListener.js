import * as firebase from "firebase";
import * as actions from "./../actions";

export const onMessagesUpdated = (dispatch) => {
    const refMessages = firebase.database().ref().child("messages");

    refMessages.orderByChild("createDate").once("value", snap => {
        dispatch(actions.addMessages({ messages: snap.val() }));
    });

    refMessages.limitToLast(1).orderByChild("createDate").on("child_added", snap => {
        dispatch(actions.createMessage({ message: snap.val() }));
    });
};
